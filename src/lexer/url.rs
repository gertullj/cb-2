use logos::{Filter, Lexer, Logos};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    #[regex(
        // r"[<][ \t\n]*a.*href=.*[>].*[<][/][ \t\r\n]*a[ \t\r\n]*[>]"
        r"<[\t\r\n]*a[^>]*>[^<]*<[ \t\r\n]*/a[ \t\r\n]*>",
        |lex| match extract_link_info(lex) {
            Some(l) => Filter::Emit(l),
            None => Filter::Skip
        }
    )]
    Link((LinkUrl, LinkText)),

    #[regex(r"<[/]?[^a][^>]*>", logos::skip)]
    #[regex(r"[^<]*", logos::skip)]
    // #[regex(r"[ \t\n\f]", logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> Option<(LinkUrl, LinkText)> {
    let match_: &str = lex.slice();

    let (_upto_href, after_href) = match_.split_once("href=\"")?;
    let (link, after_link) = after_href.split_once('\"')?;
    let (_html_block_end, content_begin) = after_link.split_once('>')?;
    let (content, _after_content) = content_begin.split_once('<')?;

    Some((LinkUrl(link.to_string()), LinkText(content.to_string())))
}
